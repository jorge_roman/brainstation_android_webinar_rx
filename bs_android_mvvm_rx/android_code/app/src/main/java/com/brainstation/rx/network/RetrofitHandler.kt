package com.brainstation.rx.network

import com.brainstation.rx.network.service.IDashboardService
import com.brainstation.rx.network.service.ILoginService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitHandler(client: OkHttpClient) {

    /** Retrofit instance [ Pointing to dummy url ] */
    private val retrofit = Retrofit.Builder()
        .baseUrl("http://demoUrl/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(client)
        .build()

    /** Services */
    val loginService: ILoginService = retrofit.create(ILoginService::class.java)

    val dashboardService: IDashboardService = retrofit.create(IDashboardService::class.java)

}
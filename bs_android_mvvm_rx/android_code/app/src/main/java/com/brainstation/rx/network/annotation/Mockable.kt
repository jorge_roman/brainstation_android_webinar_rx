package com.brainstation.rx.network.annotation

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class Mockable(val code: Int, val asset: String, val enabled: Boolean)
package com.brainstation.rx.view.login

import android.content.Context
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.PublishSubject
import android.util.Patterns.EMAIL_ADDRESS
import com.brainstation.rx.BrainStationApplication
import com.brainstation.rx.model.User
import com.brainstation.rx.network.manager.LoginManager
import io.reactivex.rxkotlin.withLatestFrom
import javax.inject.Inject


class LoginViewModel(context: Context) {

    @Inject
    lateinit var loginManager: LoginManager

    val emailInputPublishSubject: PublishSubject<String> = PublishSubject.create()
    val passwordInputPublishSubject: PublishSubject<String> = PublishSubject.create()
    val loginActionPublishSubject: PublishSubject<Boolean> = PublishSubject.create()

    lateinit var validEmailObservable: Observable<Boolean>
    lateinit var validPasswordObservable: Observable<Boolean>
    lateinit var loadingLoginObservable: Observable<Boolean>
    lateinit var loginObservable: Observable<User>

    init {
        (context.applicationContext as BrainStationApplication).dependencyInjector.inject(this)

        setEmailObservables()
        setPasswordObservables()
        setLoginObservable()
    }

    private fun setEmailObservables() {
        val emailNotEmptyObservable = emailInputPublishSubject
            .map { it.isNotEmpty() }

        val emailIsValidObservable = emailInputPublishSubject
            .map { EMAIL_ADDRESS.matcher(it).matches() }

        validEmailObservable = Observables.combineLatest(emailNotEmptyObservable, emailIsValidObservable) {
            notEmpty, validEmail -> notEmpty && validEmail
        }
    }

    private fun setPasswordObservables() {
        validPasswordObservable = passwordInputPublishSubject
            .map { it.length >= 4 }
    }

    private fun setLoginObservable() {
        loadingLoginObservable = loginActionPublishSubject
            .withLatestFrom(validEmailObservable, validPasswordObservable) { _, validEmail, validPassword -> validEmail && validPassword }
            .filter { it }

        loginObservable = loadingLoginObservable
            .withLatestFrom(emailInputPublishSubject, passwordInputPublishSubject) { _, email, password ->  Pair<String, String>(email, password) }
            .switchMap { loginData -> loginManager.verifyUser(loginData.first, loginData.second) }
    }

}
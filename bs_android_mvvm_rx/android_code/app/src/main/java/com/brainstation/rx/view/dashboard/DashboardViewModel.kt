package com.brainstation.rx.view.dashboard

import android.content.Context
import com.brainstation.rx.BrainStationApplication
import com.brainstation.rx.model.Course
import com.brainstation.rx.network.manager.DashboardManager
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import java.util.*

import javax.inject.Inject

class DashboardViewModel(context: Context) {

    @Inject
    lateinit var dashboardManager: DashboardManager

    val getMyCoursesPublishSubject: PublishSubject<Boolean> = PublishSubject.create()

    var coursesObservable: Observable<ArrayList<Course>>
    var myCoursesObservable: Observable<ArrayList<Course>>

    init {
        (context.applicationContext as BrainStationApplication).dependencyInjector.inject(this)

        coursesObservable = dashboardManager
            .getCourses()

        myCoursesObservable = getMyCoursesPublishSubject
            .switchMap{ dashboardManager.getMyCourses() }

    }

}
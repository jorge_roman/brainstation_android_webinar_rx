package com.brainstation.rx.network.service

import com.brainstation.rx.model.Course
import com.brainstation.rx.network.annotation.Mockable
import io.reactivex.Flowable
import retrofit2.http.GET

interface IDashboardService {

    @Mockable(code = 200, asset = "GET_COURSES_200.json", enabled = true)
    @GET("/courses")
    fun getCourses(): Flowable<ArrayList<Course>>

    @Mockable(code = 404, asset = "GET_USER_COURSES_404.json", enabled = true)
    @GET("/user/courses")
    fun getUserCourses(): Flowable<ArrayList<Course>>

}
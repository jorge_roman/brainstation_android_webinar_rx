package com.brainstation.rx.network.service

import com.brainstation.rx.model.User
import com.brainstation.rx.network.annotation.Mockable
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.POST

interface ILoginService {

    @Mockable(code = 200, asset = "POST_LOGIN_200.json", enabled = true)
    @POST("/user")
    fun login(@Body body: HashMap<String, String>): Flowable<User>

}
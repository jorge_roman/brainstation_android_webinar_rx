package com.brainstation.rx.model

data class User(val name: String, val accessLevel: String)
package com.brainstation.rx.network.interceptor

import android.content.Context
import android.util.Log
import com.brainstation.rx.network.annotation.Mockable
import okhttp3.*
import retrofit2.Invocation

class MockInterceptor(private val context: Context, private val delay: Long): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val invocation = request.tag(Invocation::class.java)
        val annotation = invocation?.method()?.getAnnotation(Mockable::class.java)

        return annotation
            ?.takeIf { it.enabled }
            ?.let {
                val inputStream = context.assets.open(it.asset)
                val body =
                    inputStream.reader(Charsets.UTF_8).buffered().use { file -> file.readText() }
                val mediaType = "application/json"

                sleep(delay) { exception ->
                    Log.w("MockHttpInterceptor", exception.toString())
                }

                Response.Builder()
                    .request(request)
                    .protocol(Protocol.HTTP_2)
                    .code(it.code)
                    .message(body)
                    .body(ResponseBody.create(MediaType.parse(mediaType), body))
                    .addHeader("content-type", mediaType)
                    .build()
            } ?: chain.proceed(request)
    }

    private fun sleep(delay: Long, block: (Exception) -> Unit) = try {
        Thread.sleep(delay)
    } catch (exception: Exception) {
        block(exception)
    }

}
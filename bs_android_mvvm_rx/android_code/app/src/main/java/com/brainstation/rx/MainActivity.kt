package com.brainstation.rx

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.brainstation.rx.navigation.NavigationManager
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    /** Dependency modules
     * @see [com.brainstation.rx.di.AppModule]
     */
    @Inject
    lateinit var navigationManager: NavigationManager

    /** LifeCycle */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /** Inject dependencies
         * @see [BrainStationApplication] */
        (application as BrainStationApplication).dependencyInjector.inject(this)

        /** Set app navigation controller */
        navigationManager.navController = findNavController(R.id.mainNav)
    }
}

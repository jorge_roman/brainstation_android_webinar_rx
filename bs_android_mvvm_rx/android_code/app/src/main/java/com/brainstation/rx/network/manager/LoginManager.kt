package com.brainstation.rx.network.manager

import android.content.Context
import com.brainstation.rx.BrainStationApplication
import com.brainstation.rx.model.User
import com.brainstation.rx.network.RetrofitHandler
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class LoginManager(context: Context) {

    @Inject
    lateinit var retrofitHandler: RetrofitHandler

    init {
        (context.applicationContext as BrainStationApplication).dependencyInjector.inject(this)
    }

    fun verifyUser(email: String, password: String): Observable<User> {

        val body: HashMap<String, String> = HashMap()
        body["email"] = email
        body["password"] = password

        return retrofitHandler.loginService.login(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
    }

}
package com.brainstation.rx.navigation

import androidx.navigation.NavController
import com.brainstation.rx.R
import io.reactivex.processors.PublishProcessor


class NavigationManager {

    lateinit var navController: NavController

    private var navEvent: PublishProcessor<NavigationEvents> = PublishProcessor.create()

    init {
        setNavigationObservables()
    }

    private fun setNavigationObservables() {

        navEvent.doOnNext {
                it?.let { event ->
                    when(event) {
                        NavigationEvents.DashboardFromLogin -> presentView(R.id.action_fragmentLogin_to_fragmentDashboard)
                    }
                }
            }
            .subscribe()
    }

    private fun presentView(routeId: Int) {
        navController.navigate(routeId)
    }

    fun presentDashboardFromLogin() {
        this.navEvent.onNext(NavigationEvents.DashboardFromLogin)
    }
}

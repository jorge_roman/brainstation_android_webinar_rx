package com.brainstation.rx.di

import android.content.Context
import com.brainstation.rx.navigation.NavigationManager
import com.brainstation.rx.network.RetrofitHandler
import com.brainstation.rx.network.interceptor.MockInterceptor
import com.brainstation.rx.network.manager.DashboardManager
import com.brainstation.rx.network.manager.LoginManager
import com.brainstation.rx.view.dashboard.DashboardViewModel
import com.brainstation.rx.view.login.LoginViewModel
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

@Module
class AppModule(
    private val context: Context
) {

    /** Navigation module */

    @Provides
    @Singleton
    fun providesNavigationManager(): NavigationManager = NavigationManager()

    /** Retrofit modules */

    @Provides
    fun providesMockInterceptor(): MockInterceptor = MockInterceptor(context, 3000L)

    @Provides
    fun providesOkHttpClient(mockInterceptor: MockInterceptor): OkHttpClient = OkHttpClient.Builder().addInterceptor(mockInterceptor).build()

    @Provides
    @Singleton
    fun providesRetrofitHandler(client: OkHttpClient): RetrofitHandler = RetrofitHandler(client)

    /** Login related modules */

    @Provides
    fun providesLoginViewModel(): LoginViewModel = LoginViewModel(context)

    @Provides
    fun providesLoginManager(): LoginManager = LoginManager(context)

    /** Dashboard related modules */

    @Provides
    fun providesDashboardViewModel(): DashboardViewModel = DashboardViewModel(context)

    @Provides
    fun providesDashboardManager(): DashboardManager = DashboardManager(context)

}
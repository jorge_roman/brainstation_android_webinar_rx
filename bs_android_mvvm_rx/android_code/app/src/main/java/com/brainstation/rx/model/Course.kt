package com.brainstation.rx.model

data class Course(val name: String, val instructor: String)
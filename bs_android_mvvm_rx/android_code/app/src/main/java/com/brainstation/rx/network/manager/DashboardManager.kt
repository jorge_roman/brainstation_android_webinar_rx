package com.brainstation.rx.network.manager

import android.content.Context
import com.brainstation.rx.BrainStationApplication
import com.brainstation.rx.model.Course
import com.brainstation.rx.network.RetrofitHandler
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DashboardManager(context: Context) {

    @Inject
    lateinit var retrofitHandler: RetrofitHandler

    init {
        (context.applicationContext as BrainStationApplication).dependencyInjector.inject(this)
    }

    fun getCourses(): Observable<ArrayList<Course>> {
        return retrofitHandler.dashboardService.getCourses()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
    }

    fun getMyCourses(): Observable<ArrayList<Course>> {
        return retrofitHandler.dashboardService.getUserCourses()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .toObservable()
    }

}
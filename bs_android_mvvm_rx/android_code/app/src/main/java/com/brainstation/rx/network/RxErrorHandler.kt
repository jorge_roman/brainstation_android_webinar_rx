package com.brainstation.rx.network

import android.content.Context
import android.widget.Toast
import io.reactivex.Observable
import retrofit2.HttpException


fun <T> Observable<T>.errorHandling(context: Context?, handler: IErrorHandler): Observable<T> =
    this.doOnError { throwable ->
        if (throwable is HttpException) {
            when (val httpErrorCode = throwable.code()) {
                404 -> Toast.makeText(context, "NETWORK ERROR: $httpErrorCode: NOT FOUND", Toast.LENGTH_SHORT).show()
                401 -> Toast.makeText(context, "NETWORK ERROR: $httpErrorCode: FORBIDDEN", Toast.LENGTH_SHORT).show()
                500 -> Toast.makeText(context, "NETWORK ERROR: $httpErrorCode: SERVER ERROR", Toast.LENGTH_SHORT).show()
            }
        }
        handler.cleanOnError()
    }.onErrorResumeNext { _: Throwable -> Observable.empty() }
package com.brainstation.rx.di

import com.brainstation.rx.MainActivity
import com.brainstation.rx.network.manager.DashboardManager
import com.brainstation.rx.network.manager.LoginManager
import com.brainstation.rx.view.dashboard.DashboardFragment
import com.brainstation.rx.view.dashboard.DashboardViewModel
import com.brainstation.rx.view.login.LoginFragment
import com.brainstation.rx.view.login.LoginViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    /** Where we can inject modules across the project */

    /** Main Activity */
    fun inject(mainActivity: MainActivity)

    /** Login */
    fun inject(loginFragment: LoginFragment)
    fun inject(loginViewModel: LoginViewModel)
    fun inject(loginManager: LoginManager)

    /** Dashboard */
    fun inject(dashboardFragment: DashboardFragment)
    fun inject(dashboardViewModel: DashboardViewModel)
    fun inject(dashboardRepository: DashboardManager)
}
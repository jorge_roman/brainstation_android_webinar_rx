package com.brainstation.rx.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.brainstation.rx.R
import kotlinx.android.synthetic.main.widget_button.view.*

class LoadingButton constructor(
    context: Context?,
    attrs: AttributeSet? = null
): ConstraintLayout(context, attrs) {

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_button, this)
    }

    fun showLoader(show: Boolean) {
        if (show) {
            button_loader.visibility = View.VISIBLE
            button_label.visibility = View.INVISIBLE
        } else {
            button_loader.visibility = View.INVISIBLE
            button_label.visibility = View.VISIBLE
        }
    }

}
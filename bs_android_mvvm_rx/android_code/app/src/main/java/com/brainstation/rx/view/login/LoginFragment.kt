package com.brainstation.rx.view.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.brainstation.rx.BrainStationApplication
import com.brainstation.rx.R
import com.brainstation.rx.navigation.NavigationManager
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.widget.textChanges
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject


class LoginFragment: Fragment() {

    @Inject
    lateinit var viewModel: LoginViewModel
    @Inject
    lateinit var navigationManager: NavigationManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_login, container, false)

    override fun onResume() {
        super.onResume()

        emailObservables()
        passwordObservables()
        loginObservables()
    }

    private fun emailObservables() {
        login_email_input.textChanges()
            .map { it.toString() }
            .doOnNext { input -> viewModel.emailInputPublishSubject.onNext(input) }
            .subscribe()

        viewModel.validEmailObservable
            .doOnNext { showInputError(login_email_error, it) }
            .subscribe()
    }

    private fun passwordObservables() {
        login_password_input.textChanges()
            .map { it.toString() }
            .doOnNext { input -> viewModel.passwordInputPublishSubject.onNext(input) }
            .subscribe()

        viewModel.validPasswordObservable
            .doOnNext { showInputError(login_password_error, it) }
            .subscribe()
    }

    private fun loginObservables() {
        login_btn.clicks()
            .doOnNext { viewModel.loginActionPublishSubject.onNext(true) }
            .subscribe()

        viewModel.loadingLoginObservable
            .doOnNext { login_btn.showLoader(true) }
            .subscribe()

        viewModel.loginObservable
            .doOnNext { goToDashboard() }
            .subscribe()
    }

    private fun showInputError(inputErrorView: View, isInputValid: Boolean) {
        when(isInputValid) {
            true -> inputErrorView.visibility = View.GONE
            false -> inputErrorView.visibility = View.VISIBLE
        }
    }

    private fun goToDashboard() {
        login_btn.showLoader(false)
        navigationManager.presentDashboardFromLogin()
    }

    private fun injectDependencies() {
        (requireContext().applicationContext as BrainStationApplication)
            .dependencyInjector
            .inject(this)
    }

}
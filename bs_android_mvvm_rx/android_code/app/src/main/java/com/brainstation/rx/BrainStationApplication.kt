package com.brainstation.rx

import android.app.Application
import com.brainstation.rx.di.AppComponent
import com.brainstation.rx.di.AppModule
import com.brainstation.rx.di.DaggerAppComponent

class BrainStationApplication : Application() {

    lateinit var dependencyInjector: AppComponent

    override fun onCreate() {
        super.onCreate()

        /** Allows module injection on the app context
         * @see com.brainstation.rx.di.AppComponent
         */
        dependencyInjector = DaggerAppComponent.builder()
            .appModule(AppModule(this)).build()
    }

}
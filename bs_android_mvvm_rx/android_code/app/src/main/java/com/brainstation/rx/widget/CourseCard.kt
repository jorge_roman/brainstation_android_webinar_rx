package com.brainstation.rx.widget

import android.content.Context
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.brainstation.rx.R

class CourseCard constructor(
    context: Context?
): ConstraintLayout(context) {

    init {
        LayoutInflater.from(context).inflate(R.layout.widget_course, this)
    }

}
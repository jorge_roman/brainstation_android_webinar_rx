package com.brainstation.rx.view.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.brainstation.rx.BrainStationApplication
import com.brainstation.rx.R
import com.brainstation.rx.model.Course
import com.brainstation.rx.network.IErrorHandler
import com.brainstation.rx.network.errorHandling
import com.brainstation.rx.widget.CourseCard
import com.jakewharton.rxbinding3.view.clicks
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.widget_course.view.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class DashboardFragment: Fragment(), IErrorHandler {

    @Inject
    lateinit var viewModel: DashboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_dashboard, container, false)

    override fun onResume() {
        super.onResume()

        coursesObservables()
    }

    private fun coursesObservables() {
        viewModel.coursesObservable
            .doOnNext { showCourses(it) }
            .subscribe()

        dashboard_courses_btn.clicks()
            .doOnNext {
                showLoader()
                viewModel.getMyCoursesPublishSubject.onNext(true)
            }
            .subscribe()

        viewModel.myCoursesObservable
            .doOnNext { showCourses(it) }
            .errorHandling(context, this)
            .subscribe()
    }

    private fun showCourses(courses: ArrayList<Course>) {
        courses_container.removeAllViews()

        dashboard_loader.visibility = View.GONE

        courses.forEach { course ->
            val courseCard = CourseCard(context)

            courseCard.course_name.text = course.name
            courseCard.course_instructors.text = course.instructor

            courses_container.addView(courseCard)
        }
    }

    private fun injectDependencies() {
        (requireContext().applicationContext as BrainStationApplication)
            .dependencyInjector
            .inject(this)
    }

    private fun showLoader() {
        dashboard_loader.visibility = View.VISIBLE
        courses_container.visibility = View.GONE
    }

    override fun cleanOnError() {
        dashboard_loader.visibility = View.GONE
        courses_container.visibility = View.VISIBLE
    }

}
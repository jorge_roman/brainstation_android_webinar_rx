# BrainStation: Android with Rx and MVVM #

This project serves as an introductory app to Rx and MVVM for the Android Platform, but the general concepts related to this technologies can easily extend to other platforms as well.

### What is this repository for? ###

Quick introduction to the reactive programming paradigm using ReactiveX and the MVVM (Model-View-ViewModel) software architectural pattern.

* [ReactiveX](http://reactivex.io/)
* [MVVM with Android](https://medium.com/upday-devs/android-architecture-patterns-part-3-model-view-viewmodel-e7eeee76b73b)
* [ReactiveX - Operators](http://reactivex.io/documentation/operators.html)

### Third party dependencies ###

* RxJava
* RxKotlin
* RxAndroid
* RxBinding
* Android Navigation
* Dagger
* Retrofit

### Contributors ###

* Jorge Roman
* Carlos Villalobos
